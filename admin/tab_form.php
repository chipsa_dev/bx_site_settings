<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/iblock/prolog.php");
CModule::IncludeModule('site.settings');
\Bitrix\Main\UI\Extension::load("ui.forms");
global $APPLICATION, $USER;

if ($_REQUEST['action'] == 'edit') {
	$data = SiteOptions::getField('tabs', $_REQUEST['tab_code']);
	$tabAction = 'Редактирование';
} else {
	$tabAction = 'Создание';
	if ($_REQUEST['parent']) 
		$data['parent'] = $_REQUEST['parent'];
}

$arPages = SiteOptions::getFieldsByType('pages');

$tabs = [
	[
		"DIV" => "edit1",
		"TAB" => $tabAction . " вкладки",
		"TITLE" => $tabAction . " вкладки",
	],
];

$tabControl = new CAdminTabControl("tabControl", $tabs);

CJSCore::Init(['jquery']);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_popup_admin.php");

$tabControl->Begin();

?>

<form class="js-setting-tab-form" method="post" name="intr_opt_form">
	<input type="hidden" name="type" value="tabs">
	<input type="hidden" name="old_code" value="<?= $data['code'] ?>">
	<?
	$tabControl->BeginNextTab();
	?>
	<?= bitrix_sessid_post(); ?>

	<tr>
		<td width="40%">
			<label>Название:</label>
		</td>
		<td valign="middle" width="60%">
			<div class="ui-ctl ui-ctl-textbox">
				<input type="text" class="ui-ctl-element" name="name" placeholder="Введите название страницы" value="<?= $data['name'] ?>" required>
			</div>
		</td>
	</tr>

	<tr>
		<td width="40%">
			<label>Код:</label>
		</td>
		<td valign="middle" width="60%">
			<div class="ui-ctl ui-ctl-textbox">
				<input type="text" class="ui-ctl-element" name="code" placeholder="Введите код вкладки" value="<?= $data['code'] ?>" required>
			</div>
		</td>
	</tr>

	<tr>
		<td width="40%">
			<label>Родительская страница:</label>
		</td>
		<td valign="middle" width="60%">
			<div class="ui-ctl ui-ctl-after-icon ui-ctl-dropdown">
				<div class="ui-ctl-after ui-ctl-icon-angle"></div>
				<select class="ui-ctl-element" name="parent">
					<? foreach ($arPages as $pageCode => $page) : ?>
						<option value="<?= $pageCode ?>" <?= $pageCode == $data['parent'] ? 'selected' : '' ?>><?= $page ?></option>
					<? endforeach; ?>
				</select>
			</div>
		</td>
	</tr>

	<tr class="js-error-block" style="display:none;">
		<td width="40%">
			<label></label>
		</td>
		<td valign="middle" width="60%">
			<div class="ui-ctl ui-ctl-textbox">
				<span class="required"></span>
			</div>
		</td>
	</tr>

	<? $tabControl->Buttons(); ?>

	<input type="submit" title="Сохранить" value="Сохранить">
</form>
<? $tabControl->End(); ?>

<script>
	window.onunload = refreshParent;

	function refreshParent() {
		window.opener.location.reload();
	}

	const route = "/bitrix/services/main/ajax.php";

	$(document).on('submit', '.js-setting-tab-form', function(event) {
		event.preventDefault();

		let data = $('.js-setting-tab-form').serializeArray();

		data.push({
			'name': 'action',
			'value': 'site:settings.api.option.storeTab'
		});

		$.get(route, data)
			.success(function(response) {
				if (response.status == 'success') {
					window.close();
					$('.js-error-block').hide(300);
				} else {
					$('.js-error-block span').html(response.errors[0].message);
					$('.js-error-block').show(300);
				}
			});
	});
</script>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_popup_admin.php");
