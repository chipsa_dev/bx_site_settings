<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/iblock/prolog.php");
CModule::IncludeModule('site.settings');
global $APPLICATION, $USER;

if ($_REQUEST['action'] == 'edit') {
	$data = SiteOptions::getField('pages', $_REQUEST['page_code']);
	$tabAction = 'Редактирование';
} else {
	$tabAction = 'Создание';
}

$tabs = [
	[
		"DIV" => "edit1",
		"TAB" => $tabAction . " страницы",
		"TITLE" => $tabAction . " страницы",
	],
];

$tabControl = new CAdminTabControl("tabControl", $tabs);

CJSCore::Init(['jquery']);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_popup_admin.php");

$tabControl->Begin();

?>

<style>
	[name^="opt_values_descr"] {
		display: block !important;
	}
</style>

<form class="js-setting-page-form" method="get" name="intr_opt_form">
	<input type="hidden" name="type" value="pages">
	<input type="hidden" name="old_code" value="<?= $data['code'] ?>">
	<?
	$tabControl->BeginNextTab();
	?>
	<?= bitrix_sessid_post(); ?>

	<tr>
		<td width="40%">
			<label>Название:</label>
		</td>
		<td valign="middle" width="60%">
			<div class="ui-ctl ui-ctl-textbox">
				<input type="text" class="ui-ctl-element" name="name" placeholder="Введите название страницы" value="<?= $data['name'] ?>" required>
			</div>
		</td>
	</tr>

	<tr>
		<td width="40%">
			<label>Код:</label>
		</td>
		<td valign="middle" width="60%">
			<div class="ui-ctl ui-ctl-textbox">
				<input type="text" class="ui-ctl-element" name="code" placeholder="Введите код страницы" value="<?= $data['code'] ?>" required>
			</div>
		</td>
	</tr>

	<tr class="js-error-block" style="display:none;">
		<td width="40%">
			<label></label>
		</td>
		<td valign="middle" width="60%">
			<div class="ui-ctl ui-ctl-textbox">
				<span class="required"></span>
			</div>
		</td>
	</tr>

	<? $tabControl->Buttons(); ?>

	<input type="submit" title="Сохранить" value="Сохранить">
</form>
<? $tabControl->End(); ?>

<script>
	window.onunload = refreshParent;

	function refreshParent() {
		window.opener.location.reload();
	}

	const route = "/bitrix/services/main/ajax.php";

	$(document).on('submit', '.js-setting-page-form', function(event) {
		event.preventDefault();

		let data = $('.js-setting-page-form').serializeArray();

		data.push({
			'name': 'action',
			'value': 'site:settings.api.option.storePage'
		});

		$.get(route, data)
			.success(function(response) {
				if (response.status == 'success') {
					window.close();
					$('.js-error-block').hide(300);
				} else {
					$('.js-error-block span').html(response.errors[0].message);
					$('.js-error-block').show(300);
				}
			});
	});
</script>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_popup_admin.php");
