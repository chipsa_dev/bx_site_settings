<?

use Bitrix\Main\ModuleManager,
    Bitrix\Main\Loader,
    Bitrix\Main\EventManager;

if (!ModuleManager::isModuleInstalled('site.settings'))
    return false;

if (!Loader::includeModule("site.settings"))
    return false;

$eventManager = EventManager::getInstance();
$eventManager->addEventHandler(
    "main",
    "OnBuildGlobalMenu",
    function (&$adminMenu)
    {
        $options = SiteOptions::getMenuItems();
        if (!empty($options)) {
            $items = [];
            foreach ($options as $type => $name) {
                $items[] = [
                    "text" => $name,
                    "url" => "/bitrix/admin/a_site_settings.php?type=$type",
                    "parent_menu" => "main_config"
                ];
            }
            $adminMenu["global_menu_content"]["items"][] = [
                "section" => "main_config",
                "text" => "Настройки сайта",
                "sort" => "200",
                "icon" => "sys_menu_icon",
                "items_id" => "main_config_items",
                "items" => $items
            ];
        }
    }
);
