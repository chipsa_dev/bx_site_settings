<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/iblock/prolog.php");
CModule::IncludeModule('site.settings');
global $APPLICATION, $USER;

if ($_REQUEST['action'] == 'edit') {
	$data = SiteOptions::getField('items', $_REQUEST['item_code']);
	$tabAction = 'Редактирование';
} else {
	$tabAction = 'Создание';
	if ($_REQUEST['parent']) 
		$data['parent'] = $_REQUEST['parent'];
}

$arPages = SiteOptions::getFieldsByType('pages');
$arTabs = SiteOptions::getFieldsByType('tabs');

$arr = [];

foreach ($arTabs as $key => $item) {
	$arr[$item['parent']][$key] = $item;
}

$arTabs = $arr;

$arTypes = [
	[
		'name' => 'Строка',
		'code' => 'string',
	],
	[
		'name' => 'Текст',
		'code' => 'text',
	],
	[
		'name' => 'HTML',
		'code' => 'html',
	],
	[
		'name' => 'Файл',
		'code' => 'file',
	],
	[
		'name' => 'Список',
		'code' => 'list',
	],
	[
		'name' => 'Флажок',
		'code' => 'boolean',
	],
];

$arMultiple = [
	'y',
	'n',
];

$tabs = [
	[
		"DIV" => "edit1",
		"TAB" => $tabAction . " элемента",
		"TITLE" => $tabAction . " элемента",
	],
];

$tabControl = new CAdminTabControl("tabControl", $tabs);

CJSCore::Init(['jquery']);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_popup_admin.php");

$tabControl->Begin();
?>

<form class="js-setting-item-form" method="get" name="intr_opt_form">
	<input type="hidden" name="type" value="items">
	<input type="hidden" name="old_code" value="<?= $data['code'] ?>">
	<?
	$tabControl->BeginNextTab();
	?>
	<?= bitrix_sessid_post(); ?>

	<tr>
		<td width="40%">
			<label>Название:</label>
		</td>
		<td valign="middle" width="60%">
			<div class="ui-ctl ui-ctl-textbox">
				<input type="text" class="ui-ctl-element" name="name" placeholder="Введите название страницы" value="<?= $data['name'] ?>" required>
			</div>
		</td>
	</tr>

	<tr>
		<td width="40%">
			<label>Код:</label>
		</td>
		<td valign="middle" width="60%">
			<div class="ui-ctl ui-ctl-textbox">
				<input type="text" class="ui-ctl-element" name="code" placeholder="Введите код страницы" value="<?= $data['code'] ?>" required>
			</div>
		</td>
	</tr>

	<tr>
		<td width="40%">
			<label>Родительская вкладка:</label>
		</td>
		<td valign="middle" width="60%">
			<div class="ui-ctl ui-ctl-after-icon ui-ctl-dropdown">
				<div class="ui-ctl-after ui-ctl-icon-angle"></div>
				<select class="ui-ctl-element" name="parent">
					<? foreach ($arTabs as $pageCode => $arTab) : ?>
						<optgroup label="<?= $arPages[$pageCode] ?>">
							<? foreach ($arTab as $tabCode => $tab) : ?>
								<option value="<?= $tabCode ?>" <?= $tabCode == $data['parent'] ? 'selected' : '' ?>><?= $tab['name'] ?></option>
							<? endforeach; ?>
						</optgroup>
					<? endforeach; ?>
				</select>
			</div>
		</td>
	</tr>

	<tr>
		<td width="40%">
			<label>Тип свойства:</label>
		</td>
		<td valign="middle" width="60%">
			<div class="ui-ctl ui-ctl-after-icon ui-ctl-dropdown">
				<div class="ui-ctl-after ui-ctl-icon-angle"></div>
				<select class="ui-ctl-element js-type-select" name="property_type">
					<? foreach ($arTypes as $type) : ?>
						<option value="<?= $type['code'] ?>" <?= $type['code'] == $data['type'] ? 'selected' : '' ?>><?= $type['name'] ?></option>
					<? endforeach; ?>
				</select>
			</div>
		</td>
	</tr>

	<tr class="js-multi" <?= $data['type'] == 'boolean' ? 'style="display:none;"' : '' ?>>
		<td width="40%">
			<label>Множественное:</label>
		</td>
		<td valign="middle" width="60%">
			<label class="ui-ctl ui-ctl-checkbox">
				<input type="checkbox" class="ui-ctl-element" name="multiple" <?= $data['multiple'] == 'y' ? 'checked' : '' ?>>
			</label>
		</td>
	</tr>

	<tr class="js-list" <?= $data['type'] == 'list' ? '' : 'style="display:none;"' ?>>
		<td width="40%">
			<label>Значения:</label>
		</td>
		<td valign="middle" width="60%">
			<div class="js-value-block">
				<? if (!empty($data['values'])) : ?>
					<? foreach ($data['values'] as $value) : ?>
						<div class="ui-ctl ui-ctl-textbox">
							<input type="text" class="ui-ctl-element" name="values[]" value="<?= $value ?>">
						</div>
					<? endforeach; ?>
				<? else : ?>
					<div class="ui-ctl ui-ctl-textbox">
						<input type="text" class="ui-ctl-element" name="values[]">
					</div>
				<? endif; ?>
			</div>
			<input type="button" class="js-add-value-button" title="Добавить значение" value="+">
		</td>
	</tr>

	<tr class="js-error-block" style="display:none;">
		<td width="40%">
			<label></label>
		</td>
		<td valign="middle" width="60%">
			<div class="ui-ctl ui-ctl-textbox">
				<span class="required"></span>
			</div>
		</td>
	</tr>

	<? $tabControl->Buttons(); ?>

	<input type="submit" title="Сохранить" value="Сохранить">
</form>
<? $tabControl->End(); ?>

<script>
	window.onunload = refreshParent;

	function refreshParent() {
		window.opener.location.reload();
	}

	const route = "/bitrix/services/main/ajax.php";

	(function() {
		$('.js-add-value-button').click(function() {
			let html = '<div class="ui-ctl ui-ctl-textbox">' +
				'<input type="text" class="ui-ctl-element" name="values[]">' +
				'</div>';

			$('.js-value-block').find('.ui-ctl-textbox').last().append(html);
		});
	})();

	(function() {
		$(document).on('change', '.js-type-select', function(event) {
			switch ($(this).val()) {
				case 'boolean':
					$('.js-multi').hide(300);
					$('.js-list').hide(300);
					$('.js-list input[type="text"]').val('');
					$('.js-multi input').attr('checked', false);
					break;
				case 'list':
					$('.js-list').show(300);
					$('.js-multi').show(300);
					break;
				default:
					$('.js-multi').show(300);
					$('.js-list').hide(300);
					$('.js-list input[type="text"]').val('');
					break;
			}
		});
	})();

	$(document).on('submit', '.js-setting-item-form', function(event) {
		event.preventDefault();

		let data = $('.js-setting-item-form').serializeArray();

		let check = false;

		data.push({
			'name': 'action',
			'value': 'site:settings.api.option.storeItem'
		});

		for (item in data) {
			if (data[item].name == 'multiple') {
				check = true;
			}
		}

		if (check != true || check == undefined) {
			data.push({
				'name': 'multiple',
				'value': 'off'
			});
		}

		$.get(route, data)
			.success(function(response) {
				if (response.status == 'success') {
					window.close();
					$('.js-error-block').hide(300);
				} else {
					$('.js-error-block span').html(response.errors[0].message);
					$('.js-error-block').show(300);
				}
			});
	});
</script>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_popup_admin.php");
