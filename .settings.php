<?
return [
    'controllers' => [
        'value' => [
            'namespaces' => [
                '\\Site\\Settings\\Controller' => 'api'
            ],
        ],
        'readonly' => true,
    ],
];
