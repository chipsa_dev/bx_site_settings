<?
if (is_array($value)) {
	foreach ($value as $key => $val) {
		$tmpValue[str_replace(["[n#IND#]", "[]"], "", $name) . "[" . $key . "]"] = $val;
	}
	$value = $tmpValue;
}
echo \Bitrix\Main\UI\FileInput::createInstance(array(
	"name" => $name,
	"description" => true,
	"upload" => true,
	"allowUpload" => $option['file_type'] ?? "A",
	"allowUploadExt" => $option['file_ext'] ?? '',
	"medialib" => false,
	"fileDialog" => false,
	"cloud" => false,
	"delete" => true,
	"maxCount" => strtoupper($option["multiple"]) == "Y" ? 10 : 1,
))->show(
	$value
);
