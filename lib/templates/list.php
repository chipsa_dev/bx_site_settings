<?
$isMultiple = strtoupper($option["multiple"]) == "Y";
?>
<input type="hidden" name="<?= $name ?>" value="">
<select <?= $isMultiple ? "multiple" : "" ?> name="<?= $name ?>">
	<? if (!$isMultiple) : ?>
		<option></option>
	<? endif; ?>
	<? foreach ($option["values"] as $key => $item) : ?>
		<? if ($isMultiple) : ?>
			<?= in_array($key, $value) ? $selected = "selected" : $selected = ""; ?>
		<? else : ?>
			<?= $key . "" === $value . "" ? $selected = "selected" : $selected = ""; ?>
		<? endif; ?>
		<option <?= $selected; ?> value="<?= $key ?>"><?= $item ?></option>
	<? endforeach; ?>
</select>