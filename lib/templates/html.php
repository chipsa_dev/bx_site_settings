<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("fileman");
$LHE = new CHTMLEditor;
$LHE->Show(
	[
		"name" => $name,
		"id" => $option["code"] . rand(0, 9999),
		"inputName" => $name,
		"content" => $value,
		"width" => "100%",
		"minBodyWidth" => 350,
		"normalBodyWidth" => 555,
		"height" => "270",
		"bAllowPhp" => false,
		"limitPhpAccess" => false,
		"autoResize" => false,
		"autoResizeOffset" => 40,
		"useFileDialogs" => true,
		"saveOnBlur" => true,
		"showTaskbars" => false,
		"showNodeNavi" => false,
		"askBeforeUnloadPage" => true,
		"bbCode" => false,
		"siteId" => SITE_ID,
		"controlsMap" => array(
			array("id" => "Bold", "compact" => true, "sort" => 80),
			array("id" => "Italic", "compact" => true, "sort" => 90),
			array('id' => 'Underline', 'compact' => true, 'sort' => 100),
			array("id" => "RemoveFormat", "compact" => true, "sort" => 120),
			array("separator" => true, "compact" => false, "sort" => 145),
			array("id" => "OrderedList", "compact" => true, "sort" => 150),
			array("id" => "UnorderedList", "compact" => true, "sort" => 160),
			array("id" => "AlignList", "compact" => false, "sort" => 190),
			array("separator" => true, "compact" => false, "sort" => 200),
			array("id" => "InsertLink", "compact" => true, "sort" => 210),
			array("id" => "InsertImage", "compact" => false, "sort" => 220),
			array('id' => 'BbCode',  'compact' => true, 'sort' => 340),
		)
	]
);
