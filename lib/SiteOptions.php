<?

use Bitrix\Main\Config\Option;

class SiteOptions
{
	public static $name = "opt_values";
	private static $template = '<tr>
		<td width="40%">
			#NAME#
			<i class="copyCode" data-code="#SCODE#"></i>
		</td>
		<td width="60%">
			#FIELD#
		</td>
	</tr>';
	private static $multiple = '<tr>
		<td width="40%"></td>
		<td width="60%">
			<input type="button" class="js-add-option-row" value="Добавить" data-code="#CODE#">
		</td>
	</tr>';
	private static $items;

	public static function isShowMultiple($option)
	{
		$type = $option["type"];
		if (
			$type == "boolean"
			|| $type == "list"
			|| $type == "file"
		)
			return false;

		return true;
	}

	public static function getMenuItems()
	{
		$result = [];
		$options = self::getOptions();

		foreach ($options["pages"] as $key => $vals) {
			$result[$key] = $vals;
		}

		return $result;
	}

	public static function getFields($type)
	{
		$options = self::getOptions();

		$fields['name'] = $options['pages'][$type];

		foreach ($options['tabs'] as $code => $tab) {
			if ($tab['parent'] == $type) {
				foreach ($options['items'] as $itemCode => $item) {
					if ($item['parent'] == $code) {
						$item['code'] = $itemCode;
						$tab['items'][$itemCode] = $item;
						self::$items[$itemCode] = $item;
					}
				}
				$fields['tabs'][$code] = $tab;
			}
		}
		return $fields;
	}

	public static function getField($type, $code)
	{
		$options = self::getOptions();

		if (is_array($options[$type][$code])) {
			$field = $options[$type][$code];
		} else {
			$field['name'] = $options[$type][$code];
		}
		$field['code'] = $code;

		return $field;
	}

	public static function getFieldsByType($type)
	{
		$options = self::getOptions();

		$result = $options[$type];

		return $result;
	}

	public static function getOption($code)
	{
		$result = [];
		$options = self::getOptions();
		foreach ($options["items"] as $optionCode => $option) {
			if ($code == $optionCode) {
				$option["code"] = $optionCode;
				$result = $option;
			}
		}

		return $result;
	}

	public static function getEmptyInput($option)
	{
		$optionField = "";
		$name = self::$name . "[" . $option["code"] . "]";
		if (strtoupper($option["multiple"]) == "Y") {
			if ($option["type"] == "file") {
				$name .=  "[n#IND#]";
			} else {
				$name .= "[]";
			}
		}

		ob_start();
		include dirname(__DIR__, 1) . "/lib/templates/" . $option["type"] . ".php";
		$optionField .= "<div>" . ob_get_contents() . "</div>";
		ob_end_clean();

		return $optionField;
	}

	public static function getFieldInput($option)
	{
		$values = self::getOptionValue($option);
		$optionField = "";

		if (is_array($values) && self::isShowMultiple($option)) {
			foreach ($values as $key => $value) {
				$name = self::$name . "[" . $option["code"] . "][]";
				ob_start();
				include dirname(__DIR__, 1) . "/lib/templates/" . $option["type"] . ".php";
				$optionField .= "<div>" . ob_get_contents() . "</div>";
				ob_end_clean();
			}
			$value = "";
			$optionField .= self::getEmptyInput($option);
		} elseif ($values) {
			$value = $values;
			$name = self::$name . "[" . $option["code"] . "]";
			if (strtoupper($option["multiple"]) == "Y") {
				if ($option["type"] == "file") {
					$name = self::$name . "[" . $option["code"] . "][n#IND#]";
				} else {
					$name = self::$name . "[" . $option["code"] . "][]";
				}
			}
			ob_start();
			include dirname(__DIR__, 1) . "/lib/templates/" . $option["type"] . ".php";
			$optionField .= "<div>" . ob_get_contents() . "</div>";
			ob_end_clean();
		} else {
			$optionField .= self::getEmptyInput($option);
		}

		return $optionField;
	}

	public static function getFieldInputMultiple($option)
	{
		return str_replace(["#NAME#", "#FIELD#"], ["", self::getEmptyInput($option)], self::$template);
	}

	public static function getFieldTemplate($option)
	{
		$template = self::$template;
		$optionField = self::getFieldInput($option);

		$template = str_replace("#NAME#", $option["name"], $template);
		$template = str_replace("#FIELD#", $optionField, $template);
		$template = str_replace("#SCODE#", $option["code"], $template);
		if (strtoupper($option["multiple"]) == "Y") {
			if (self::isShowMultiple($option))
				$template .= str_replace("#CODE#", $option["code"], self::$multiple);
		}

		return $template;
	}

	public static function getValue($code)
	{
		$option = self::getOption($code);
		if ($option)
			return self::getOptionValue($option);
	}

	public static function formatOutputOptionValue($option, $module = "site_settings")
	{
		$result = Option::get($module, $option["code"]);

		switch ($option['type']) {
			case 'datetime':
				$result = intval($result);
				break;

			case 'boolean':
				$result = $result == 'on' ? true : false;
				break;
		}

		return $result;
	}

	public static function getOptionValue($option, $module = "site_settings")
	{
		if (strtoupper($option["multiple"]) == "Y") {
			$result = [];
			$i = 0;
			do {
				$val = Option::get($module, $option["code"] . "[$i]");
				if ($val !== '')
					$result[$i] = $val;
				$i++;
			} while ($val !== '');
		} else {
			$result = self::formatOutputOptionValue($option);
		}

		return $result;
	}

	private static function getOptions()
	{
		return include($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/settings/settings.php");
	}

	public static function setOptionValue($optionName, $value, $module = "site_settings")
	{
		if (is_array($value)) {
			$i = 0;
			do {
				$val = Option::get($module, $optionName . "[$i]");
				if ($val !== '')
					Option::delete($module, ["name" => $optionName . "[" . $i . "]"]);
				$i++;
			} while ($val !== '');

			$key = 0;
			foreach ($value as $val) {
				if ($val !== '') {
					Option::set($module, $optionName . "[" . $key . "]", $val);
					$key++;
				}
			}
		} else {
			if (self::$items[$optionName]['type'] == 'datetime') {
				$d = DateTime::createFromFormat('d.m.Y H:i:s', $value);
				if (!empty($d)) {
					$value = $d->getTimestamp();
				}
			}

			Option::set($module, $optionName, $value);
		}
	}

	public static function saveFile($optionName, $value, $module = "site_settings")
	{
		if (is_array($value) && isset($value["name"])) {
			$value["tmp_name"] = $_SERVER["DOCUMENT_ROOT"] . "/upload/tmp" . $value["tmp_name"];
			$value["MODULE_ID"] = $module;
			$fileID = CFile::SaveFile($value, "index");
			if ($fileID) {
				self::setOptionValue($optionName, $fileID);
			}
		} elseif (is_array($value)) {
			$counter = 0;
			foreach ($value as $key => $arFile) {
				if (is_array($arFile)) {
					$value = $arFile;
					$value["tmp_name"] = $_SERVER["DOCUMENT_ROOT"] . "/upload/tmp" . $value["tmp_name"];
					$value["MODULE_ID"] = $module;
					$value["description"] = $_REQUEST[self::$name . '_descr'][$optionName][$key];
					$fileID = CFile::SaveFile($value, "index");
					if ($fileID) {
						self::setOptionValue($optionName . "[" . $counter . "]", $fileID);
					}
				} else {
					CFile::UpdateDesc($arFile, $_REQUEST[self::$name . '_descr'][$optionName][$key]);
				}
				$counter++;
			}
		}
	}
}
