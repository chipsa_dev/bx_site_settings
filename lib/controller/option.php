<?php

namespace Site\Settings\Controller;

use Bitrix\Main\Engine\Controller;
use Bitrix\Main\Engine\ActionFilter;
use \Bitrix\Main\Error;
use SiteOptions;

class Option extends Controller
{
	/**
	 * @return array
	 */
	public function configureActions()
	{
		return [
			'storeItem' => [
				'prefilters' => [
					new ActionFilter\HttpMethod([
						ActionFilter\HttpMethod::METHOD_GET
					]),
					new ActionFilter\Authentication,
				]
			],
			'storeTab' => [
				'prefilters' => [
					new ActionFilter\HttpMethod([
						ActionFilter\HttpMethod::METHOD_GET
					]),
					new ActionFilter\Authentication,
				]
			],
			'storePage' => [
				'prefilters' => [
					new ActionFilter\HttpMethod([
						ActionFilter\HttpMethod::METHOD_GET
					]),
					new ActionFilter\Authentication,
				]
			],
			'delete' => [
				'prefilters' => [
					new ActionFilter\HttpMethod([
						ActionFilter\HttpMethod::METHOD_GET
					]),
					new ActionFilter\Authentication,
				]
			],
		];
	}

	public function storeItemAction($name, $code, $property_type, $parent, $multiple, $values, $type, $old_code)
	{
		if (!$this->checkCode($code, $old_code)) {
			$this->addError(new Error('Код должен быть уникальным', 418));

			return null;
		}

		$data = [
			'name' => $name,
			'type' => $property_type,
			'parent' => $parent,
		];

		switch ($multiple) {
			case 'on':
				$data['multiple'] = 'y';
				break;
			case 'off':
				$data['multiple'] = 'n';
				break;
		}

		$values = array_filter($values);

		if (!empty($values)) $data['values'] = $values;

		if ($this->setDataArray($data, $code, $type, $old_code))
			return ['success' => true];
	}

	public function storeTabAction($name, $code, $parent, $type, $old_code)
	{
		if (!$this->checkCode($code, $old_code)) {
			$this->addError(new Error('Код должен быть уникальным', 418));

			return null;
		}

		$data = [
			'name' => $name,
			'parent' => $parent,
		];

		if ($this->setDataArray($data, $code, $type, $old_code))
			return ['success' => true];
	}

	public function storePageAction($name, $code, $type, $old_code)
	{
		if (!$this->checkCode($code, $old_code)) {
			$this->addError(new Error('Код должен быть уникальным', 418));

			return null;
		}

		$data = $name;

		if ($this->setDataArray($data, $code, $type, $old_code))
			return ['success' => true];
	}

	public function deleteAction($type, $code)
	{
		$data = $this->getSettingConfig();

		unset($data[$type][$code]);

		switch ($type) {
			case 'pages':
				foreach ($data['tabs'] as $tabCode => $tab) {
					if ($tab['parent'] == $code) {
						foreach ($data['items'] as $itemCode => $item) {
							if ($item['parent'] == $tabCode) unset($data['items'][$itemCode]);
						}
						unset($data['tabs'][$tabCode]);
					}
				}
				break;
			case 'tabs':
				foreach ($data['items'] as $itemCode => $item) {
					if ($item['parent'] == $data['tabs'][$code]) unset($data['items'][$itemCode]);
				}
				break;
		}

		$this->setSettingConfig($data);

		return ['success' => true];
	}

	private function getSettingConfig()
	{
		return include($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/settings/settings.php');
	}

	private function setDataArray($formData, $code, $type, $old_code)
	{
		$data = $this->getSettingConfig();

		if (empty($old_code)) {
			$data[$type][$code] = $formData;
		} else {
			if ($old_code != $code) {
				unset($data[$type][$old_code]);
			}
			$data[$type][$code] = $formData;
		}

		$this->setSettingConfig($data);

		return true;
	}

	private function setSettingConfig(array $data)
	{
		$file = $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/settings/settings.php';
		file_put_contents($file, '<? return ' . var_export($data, true) . ';');
		return true;
	}

	/**
	 * TODO: make it into filter
	 */
	private function checkCode($code, $old_code)
	{
		if ($code == $old_code) return true;

		$arData = $this->getSettingConfig();
		$arCode = [];

		foreach ($arData as $data) {
			$arCode = array_merge($arCode, array_keys($data));
		}

		if (in_array($code, $arCode)) {
			return false;
		}

		return true;
	}
}
