<?php
$module_id = 'site.settings';

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

if (!Loader::includeModule($module_id)) {
    return;
}
\Bitrix\Main\UI\Extension::load("ui.forms");
\Bitrix\Main\UI\Extension::load("ui.buttons");
\Bitrix\Main\UI\Extension::load("ui.icons");
global $APPLICATION;

$context = Application::getInstance()->getContext();
$request = $context->getRequest();
$docRoot = $context->getServer()->getDocumentRoot();

Loc::loadMessages($docRoot . BX_ROOT . "/modules/main/options.php");
Loc::loadMessages(__FILE__);

if ($APPLICATION->GetGroupRight($module_id) < "S") {
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

$optionData = SiteOptions::getMenuItems();
$arData = [];

foreach ($optionData as $key => &$data) {
    $data = SiteOptions::getFields($key);
}

$tabs = [
    [
        "DIV" => "edit1",
        "TAB" => "Настройки",
        "TITLE" => "Настройка параметров модуля",
    ],
];

$tabControl = new CAdminTabControl("tabControl", $tabs);

$backUrl = $request->get('back_url_settings');

CJSCore::Init(['jquery']);
?>
<style>
    .option-page {
        margin-bottom: 2rem;
    }

    .option-wrapper {
        display: grid;
        grid-template-columns: 33% 33% 33%;
        grid-gap: 5px;
        margin-bottom: 1rem;
    }

    .option-box {
        border-radius: 5px;
        padding: 10px;
        font-size: 130%;
        border: solid 1px;
        border-color: pink;
    }

    .option-box ul {
        margin: unset;
        margin-bottom: 1rem;
    }

    .option-tab-title {
        font-size: 150%;
    }

    .bx-sender-letter-template-selector {
        background: #eef2f4;
        margin-top: 1rem;
        border-bottom: 1px solid rgba(82, 92, 105, .09);
        /* min-height: calc(100vh - 75px); */
    }

    .sender-tpl {
        padding: 0 15px;
        overflow: hidden;
        color: #333;
        font: 18px/24px 'OpenSans-Regular', "Helvetica Neue", Arial, Helvetica, sans-serif;
    }

    .sender-tpl-section {
        display: block;
        padding-top: 1rem;
    }

    .sender-tpl-title {
        display: block;
        margin: 0 0 18px 0;
        padding: 0 0 17px 0;
        border-bottom: 1px solid rgba(82, 92, 105, .09);
    }

    .sender-tpl-items {
        font-size: 0;
        margin: 0 -20px;
    }

    .sender-tpl-item {
        display: inline-block;
        vertical-align: top;
    }

    .sender-tpl-content,
    .sender-tpl-items-show-all .sender-tpl-item-hidden .sender-tpl-content {
        display: block;
        overflow: hidden;
        position: relative;
        opacity: 1;
        /* height: 208px; */
        background: #fff;
        margin: 0 20px 30px 20px;
        border-radius: 1px;
        font-size: 12px;
        box-shadow: 0 1px 2px rgba(0, 0, 0, .15);
        transition: .2s;
    }

    .sender-tpl-item-title {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        overflow: hidden;
        padding: 0 12px;
        line-height: 38px;
        height: 38px;
        white-space: nowrap;
        text-overflow: ellipsis;
        font-size: 12px;
        font-family: OpenSans-SemiBold, Helvetca, Arial, sans-sarif, serif;
    }

    .sender-tpl-item-title-name {
        -webkit-box-flex: 1;
        -ms-flex: 1;
        flex: 1;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }

    .sender-tpl-item-title-demo {
        display: none;
        float: right;
        line-height: 38px;
        height: 38px;
        padding: 0 12px;
        color: #535c69;
        font-size: 11px;
        margin-right: -12px;
        border-radius: 0 1px 0 0;
        font-family: OpenSans-Bold, Helvetca, Arial, sans-sarif, serif;
    }

    .sender-tpl-mail .sender-tpl-item-image {
        background-image: url('data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20xmlns%3Axlink%3D%22http%3A//www.w3.org/1999/xlink%22%20width%3D%22101%22%20height%3D%22101%22%20viewBox%3D%220%200%20101%20101%22%3E%0A%20%20%3Cdefs%3E%0A%20%20%20%20%3Cpolygon%20id%3D%22b%22%20points%3D%2232%2037%200%2062%2064%2062%22/%3E%0A%20%20%20%20%3Cfilter%20id%3D%22a%22%20width%3D%22103.1%25%22%20height%3D%22116%25%22%20x%3D%22-1.6%25%22%20y%3D%22-4%25%22%20filterUnits%3D%22objectBoundingBox%22%3E%0A%20%20%20%20%20%20%3CfeOffset%20dy%3D%222%22%20in%3D%22SourceAlpha%22%20result%3D%22shadowOffsetOuter1%22/%3E%0A%20%20%20%20%20%20%3CfeColorMatrix%20in%3D%22shadowOffsetOuter1%22%20values%3D%220%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200.0912307518%200%22/%3E%0A%20%20%20%20%3C/filter%3E%0A%20%20%3C/defs%3E%0A%20%20%3Cg%20fill%3D%22none%22%20fill-rule%3D%22evenodd%22%3E%0A%20%20%20%20%3Cpath%20fill%3D%22%23FFFFFF%22%20fill-rule%3D%22nonzero%22%20d%3D%22M49.9793814%2C0%20C78.3904004%2C0%20101%2C22.6092051%20101%2C51.0206186%20C101%2C78.3904004%2078.3904004%2C101%2049.9793814%2C101%20C22.6092051%2C101%200%2C78.3904004%200%2C51.0206186%20C0%2C22.6092051%2022.6095996%2C0%2049.9793814%2C0%20Z%22%20opacity%3D%22.6%22/%3E%0A%20%20%20%20%3Cg%20transform%3D%22translate%2818%2018%29%22%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%2347C1D1%22%20fill-rule%3D%22nonzero%22%20points%3D%2264%2024%200%2024%2032%205%22/%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%2347C1D1%22%20fill-rule%3D%22nonzero%22%20points%3D%220%2024%2064%2024%2064%2061%200%2061%22/%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%23FFFFFF%22%20fill-rule%3D%22nonzero%22%20points%3D%2247%200%2056%209%2056%2061%206%2061%206%200%22/%3E%0A%20%20%20%20%20%20%3Cpath%20fill%3D%22%23FFA900%22%20d%3D%22M13%2C8%20L21%2C8%20L21%2C8%20C21.5522847%2C8%2022%2C8.44771525%2022%2C9%20L22%2C17%20L22%2C17%20C22%2C17.5522847%2021.5522847%2C18%2021%2C18%20L13%2C18%20L13%2C18%20C12.4477153%2C18%2012%2C17.5522847%2012%2C17%20L12%2C9%20L12%2C9%20C12%2C8.44771525%2012.4477153%2C8%2013%2C8%20Z%22%20opacity%3D%22.5%22/%3E%0A%20%20%20%20%20%20%3Cpath%20fill%3D%22%23A8ADB4%22%20d%3D%22M25.5%208L45.5%208%2045.5%208C45.7761424%208%2046%208.22385763%2046%208.5L46%208.5%2046%208.5C46%208.77614237%2045.7761424%209%2045.5%209L25.5%209%2025.5%209C25.2238576%209%2025%208.77614237%2025%208.5L25%208.5%2025%208.5C25%208.22385763%2025.2238576%208%2025.5%208zM25.5%2011L45.5%2011%2045.5%2011C45.7761424%2011%2046%2011.2238576%2046%2011.5L46%2011.5%2046%2011.5C46%2011.7761424%2045.7761424%2012%2045.5%2012L25.5%2012%2025.5%2012C25.2238576%2012%2025%2011.7761424%2025%2011.5L25%2011.5%2025%2011.5C25%2011.2238576%2025.2238576%2011%2025.5%2011zM12.5%2021L45.5%2021%2045.5%2021C45.7761424%2021%2046%2021.2238576%2046%2021.5L46%2021.5%2046%2021.5C46%2021.7761424%2045.7761424%2022%2045.5%2022L12.5%2022%2012.5%2022C12.2238576%2022%2012%2021.7761424%2012%2021.5L12%2021.5%2012%2021.5C12%2021.2238576%2012.2238576%2021%2012.5%2021zM25.5%2014L45.5%2014%2045.5%2014C45.7761424%2014%2046%2014.2238576%2046%2014.5L46%2014.5%2046%2014.5C46%2014.7761424%2045.7761424%2015%2045.5%2015L25.5%2015%2025.5%2015C25.2238576%2015%2025%2014.7761424%2025%2014.5L25%2014.5%2025%2014.5C25%2014.2238576%2025.2238576%2014%2025.5%2014zM12.5%2024L45.5%2024%2045.5%2024C45.7761424%2024%2046%2024.2238576%2046%2024.5L46%2024.5%2046%2024.5C46%2024.7761424%2045.7761424%2025%2045.5%2025L12.5%2025%2012.5%2025C12.2238576%2025%2012%2024.7761424%2012%2024.5L12%2024.5%2012%2024.5C12%2024.2238576%2012.2238576%2024%2012.5%2024zM25.5%2017L33.5%2017%2033.5%2017C33.7761424%2017%2034%2017.2238576%2034%2017.5L34%2017.5%2034%2017.5C34%2017.7761424%2033.7761424%2018%2033.5%2018L25.5%2018%2025.5%2018C25.2238576%2018%2025%2017.7761424%2025%2017.5L25%2017.5%2025%2017.5C25%2017.2238576%2025.2238576%2017%2025.5%2017zM12.5%2027L45.5%2027%2045.5%2027C45.7761424%2027%2046%2027.2238576%2046%2027.5L46%2027.5%2046%2027.5C46%2027.7761424%2045.7761424%2028%2045.5%2028L12.5%2028%2012.5%2028C12.2238576%2028%2012%2027.7761424%2012%2027.5L12%2027.5%2012%2027.5C12%2027.2238576%2012.2238576%2027%2012.5%2027zM12.5%2030L26.5%2030%2026.5%2030C26.7761424%2030%2027%2030.2238576%2027%2030.5L27%2030.5%2027%2030.5C27%2030.7761424%2026.7761424%2031%2026.5%2031L12.5%2031%2012.5%2031C12.2238576%2031%2012%2030.7761424%2012%2030.5L12%2030.5%2012%2030.5C12%2030.2238576%2012.2238576%2030%2012.5%2030z%22%20opacity%3D%22.5%22/%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%23E0E3E9%22%20fill-rule%3D%22nonzero%22%20points%3D%2247%200%2047%209%2056%209%22%20opacity%3D%22.341%22/%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%2355D0E0%22%20fill-rule%3D%22nonzero%22%20points%3D%220%2025%2032%2043%200%2062%22/%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%2355D0E0%22%20fill-rule%3D%22nonzero%22%20points%3D%2264%2025%2032%2043%2064%2062%22/%3E%0A%20%20%20%20%20%20%3Cg%20fill-rule%3D%22nonzero%22%3E%0A%20%20%20%20%20%20%20%20%3Cuse%20fill%3D%22black%22%20filter%3D%22url%28%23a%29%22%20xlink%3Ahref%3D%22%23b%22/%3E%0A%20%20%20%20%20%20%20%20%3Cuse%20fill%3D%22%2347C1D1%22%20fill-rule%3D%22evenodd%22%20xlink%3Ahref%3D%22%23b%22/%3E%0A%20%20%20%20%20%20%3C/g%3E%0A%20%20%20%20%3C/g%3E%0A%20%20%3C/g%3E%0A%3C/svg%3E%0A');
    }

    .sender-tpl-item-image {
        display: block;
        overflow: hidden;
        position: relative;
        height: 120px;
        background: #f5f9fa url('data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20xmlns%3Axlink%3D%22http%3A//www.w3.org/1999/xlink%22%20width%3D%22101%22%20height%3D%22101%22%20viewBox%3D%220%200%20101%20101%22%3E%0A%20%20%3Cdefs%3E%0A%20%20%20%20%3Cpolygon%20id%3D%22b%22%20points%3D%2232%2037%200%2062%2064%2062%22/%3E%0A%20%20%20%20%3Cfilter%20id%3D%22a%22%20width%3D%22103.1%25%22%20height%3D%22116%25%22%20x%3D%22-1.6%25%22%20y%3D%22-4%25%22%20filterUnits%3D%22objectBoundingBox%22%3E%0A%20%20%20%20%20%20%3CfeOffset%20dy%3D%222%22%20in%3D%22SourceAlpha%22%20result%3D%22shadowOffsetOuter1%22/%3E%0A%20%20%20%20%20%20%3CfeColorMatrix%20in%3D%22shadowOffsetOuter1%22%20values%3D%220%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200.0912307518%200%22/%3E%0A%20%20%20%20%3C/filter%3E%0A%20%20%3C/defs%3E%0A%20%20%3Cg%20fill%3D%22none%22%20fill-rule%3D%22evenodd%22%3E%0A%20%20%20%20%3Cpath%20fill%3D%22%23FFFFFF%22%20fill-rule%3D%22nonzero%22%20d%3D%22M49.9793814%2C0%20C78.3904004%2C0%20101%2C22.6092051%20101%2C51.0206186%20C101%2C78.3904004%2078.3904004%2C101%2049.9793814%2C101%20C22.6092051%2C101%200%2C78.3904004%200%2C51.0206186%20C0%2C22.6092051%2022.6095996%2C0%2049.9793814%2C0%20Z%22%20opacity%3D%22.6%22/%3E%0A%20%20%20%20%3Cg%20transform%3D%22translate%2818%2018%29%22%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%2347C1D1%22%20fill-rule%3D%22nonzero%22%20points%3D%2264%2024%200%2024%2032%205%22/%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%2347C1D1%22%20fill-rule%3D%22nonzero%22%20points%3D%220%2024%2064%2024%2064%2061%200%2061%22/%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%23FFFFFF%22%20fill-rule%3D%22nonzero%22%20points%3D%2247%200%2056%209%2056%2061%206%2061%206%200%22/%3E%0A%20%20%20%20%20%20%3Cpath%20fill%3D%22%23FFA900%22%20d%3D%22M13%2C8%20L21%2C8%20L21%2C8%20C21.5522847%2C8%2022%2C8.44771525%2022%2C9%20L22%2C17%20L22%2C17%20C22%2C17.5522847%2021.5522847%2C18%2021%2C18%20L13%2C18%20L13%2C18%20C12.4477153%2C18%2012%2C17.5522847%2012%2C17%20L12%2C9%20L12%2C9%20C12%2C8.44771525%2012.4477153%2C8%2013%2C8%20Z%22%20opacity%3D%22.5%22/%3E%0A%20%20%20%20%20%20%3Cpath%20fill%3D%22%23A8ADB4%22%20d%3D%22M25.5%208L45.5%208%2045.5%208C45.7761424%208%2046%208.22385763%2046%208.5L46%208.5%2046%208.5C46%208.77614237%2045.7761424%209%2045.5%209L25.5%209%2025.5%209C25.2238576%209%2025%208.77614237%2025%208.5L25%208.5%2025%208.5C25%208.22385763%2025.2238576%208%2025.5%208zM25.5%2011L45.5%2011%2045.5%2011C45.7761424%2011%2046%2011.2238576%2046%2011.5L46%2011.5%2046%2011.5C46%2011.7761424%2045.7761424%2012%2045.5%2012L25.5%2012%2025.5%2012C25.2238576%2012%2025%2011.7761424%2025%2011.5L25%2011.5%2025%2011.5C25%2011.2238576%2025.2238576%2011%2025.5%2011zM12.5%2021L45.5%2021%2045.5%2021C45.7761424%2021%2046%2021.2238576%2046%2021.5L46%2021.5%2046%2021.5C46%2021.7761424%2045.7761424%2022%2045.5%2022L12.5%2022%2012.5%2022C12.2238576%2022%2012%2021.7761424%2012%2021.5L12%2021.5%2012%2021.5C12%2021.2238576%2012.2238576%2021%2012.5%2021zM25.5%2014L45.5%2014%2045.5%2014C45.7761424%2014%2046%2014.2238576%2046%2014.5L46%2014.5%2046%2014.5C46%2014.7761424%2045.7761424%2015%2045.5%2015L25.5%2015%2025.5%2015C25.2238576%2015%2025%2014.7761424%2025%2014.5L25%2014.5%2025%2014.5C25%2014.2238576%2025.2238576%2014%2025.5%2014zM12.5%2024L45.5%2024%2045.5%2024C45.7761424%2024%2046%2024.2238576%2046%2024.5L46%2024.5%2046%2024.5C46%2024.7761424%2045.7761424%2025%2045.5%2025L12.5%2025%2012.5%2025C12.2238576%2025%2012%2024.7761424%2012%2024.5L12%2024.5%2012%2024.5C12%2024.2238576%2012.2238576%2024%2012.5%2024zM25.5%2017L33.5%2017%2033.5%2017C33.7761424%2017%2034%2017.2238576%2034%2017.5L34%2017.5%2034%2017.5C34%2017.7761424%2033.7761424%2018%2033.5%2018L25.5%2018%2025.5%2018C25.2238576%2018%2025%2017.7761424%2025%2017.5L25%2017.5%2025%2017.5C25%2017.2238576%2025.2238576%2017%2025.5%2017zM12.5%2027L45.5%2027%2045.5%2027C45.7761424%2027%2046%2027.2238576%2046%2027.5L46%2027.5%2046%2027.5C46%2027.7761424%2045.7761424%2028%2045.5%2028L12.5%2028%2012.5%2028C12.2238576%2028%2012%2027.7761424%2012%2027.5L12%2027.5%2012%2027.5C12%2027.2238576%2012.2238576%2027%2012.5%2027zM12.5%2030L26.5%2030%2026.5%2030C26.7761424%2030%2027%2030.2238576%2027%2030.5L27%2030.5%2027%2030.5C27%2030.7761424%2026.7761424%2031%2026.5%2031L12.5%2031%2012.5%2031C12.2238576%2031%2012%2030.7761424%2012%2030.5L12%2030.5%2012%2030.5C12%2030.2238576%2012.2238576%2030%2012.5%2030z%22%20opacity%3D%22.5%22/%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%23E0E3E9%22%20fill-rule%3D%22nonzero%22%20points%3D%2247%200%2047%209%2056%209%22%20opacity%3D%22.341%22/%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%2355D0E0%22%20fill-rule%3D%22nonzero%22%20points%3D%220%2025%2032%2043%200%2062%22/%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%2355D0E0%22%20fill-rule%3D%22nonzero%22%20points%3D%2264%2025%2032%2043%2064%2062%22/%3E%0A%20%20%20%20%20%20%3Cg%20fill-rule%3D%22nonzero%22%3E%0A%20%20%20%20%20%20%20%20%3Cuse%20fill%3D%22black%22%20filter%3D%22url%28%23a%29%22%20xlink%3Ahref%3D%22%23b%22/%3E%0A%20%20%20%20%20%20%20%20%3Cuse%20fill%3D%22%2347C1D1%22%20fill-rule%3D%22evenodd%22%20xlink%3Ahref%3D%22%23b%22/%3E%0A%20%20%20%20%20%20%3C/g%3E%0A%20%20%20%20%3C/g%3E%0A%20%20%3C/g%3E%0A%3C/svg%3E%0A') center center no-repeat;
        background-image: url("data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20xmlns%3Axlink%3D%22http%3A//www.w3.org/1999/xlink%22%20width%3D%22101%22%20height%3D%22101%22%20viewBox%3D%220%200%20101%20101%22%3E%0A%20%20%3Cdefs%3E%0A%20%20%20%20%3Cpolygon%20id%3D%22b%22%20points%3D%2232%2037%200%2062%2064%2062%22/%3E%0A%20%20%20%20%3Cfilter%20id%3D%22a%22%20width%3D%22103.1%25%22%20height%3D%22116%25%22%20x%3D%22-1.6%25%22%20y%3D%22-4%25%22%20filterUnits%3D%22objectBoundingBox%22%3E%0A%20%20%20%20%20%20%3CfeOffset%20dy%3D%222%22%20in%3D%22SourceAlpha%22%20result%3D%22shadowOffsetOuter1%22/%3E%0A%20%20%20%20%20%20%3CfeColorMatrix%20in%3D%22shadowOffsetOuter1%22%20values%3D%220%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200%200.0912307518%200%22/%3E%0A%20%20%20%20%3C/filter%3E%0A%20%20%3C/defs%3E%0A%20%20%3Cg%20fill%3D%22none%22%20fill-rule%3D%22evenodd%22%3E%0A%20%20%20%20%3Cpath%20fill%3D%22%23FFFFFF%22%20fill-rule%3D%22nonzero%22%20d%3D%22M49.9793814%2C0%20C78.3904004%2C0%20101%2C22.6092051%20101%2C51.0206186%20C101%2C78.3904004%2078.3904004%2C101%2049.9793814%2C101%20C22.6092051%2C101%200%2C78.3904004%200%2C51.0206186%20C0%2C22.6092051%2022.6095996%2C0%2049.9793814%2C0%20Z%22%20opacity%3D%22.6%22/%3E%0A%20%20%20%20%3Cg%20transform%3D%22translate%2818%2018%29%22%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%2347C1D1%22%20fill-rule%3D%22nonzero%22%20points%3D%2264%2024%200%2024%2032%205%22/%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%2347C1D1%22%20fill-rule%3D%22nonzero%22%20points%3D%220%2024%2064%2024%2064%2061%200%2061%22/%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%23FFFFFF%22%20fill-rule%3D%22nonzero%22%20points%3D%2247%200%2056%209%2056%2061%206%2061%206%200%22/%3E%0A%20%20%20%20%20%20%3Cpath%20fill%3D%22%23FFA900%22%20d%3D%22M13%2C8%20L21%2C8%20L21%2C8%20C21.5522847%2C8%2022%2C8.44771525%2022%2C9%20L22%2C17%20L22%2C17%20C22%2C17.5522847%2021.5522847%2C18%2021%2C18%20L13%2C18%20L13%2C18%20C12.4477153%2C18%2012%2C17.5522847%2012%2C17%20L12%2C9%20L12%2C9%20C12%2C8.44771525%2012.4477153%2C8%2013%2C8%20Z%22%20opacity%3D%22.5%22/%3E%0A%20%20%20%20%20%20%3Cpath%20fill%3D%22%23A8ADB4%22%20d%3D%22M25.5%208L45.5%208%2045.5%208C45.7761424%208%2046%208.22385763%2046%208.5L46%208.5%2046%208.5C46%208.77614237%2045.7761424%209%2045.5%209L25.5%209%2025.5%209C25.2238576%209%2025%208.77614237%2025%208.5L25%208.5%2025%208.5C25%208.22385763%2025.2238576%208%2025.5%208zM25.5%2011L45.5%2011%2045.5%2011C45.7761424%2011%2046%2011.2238576%2046%2011.5L46%2011.5%2046%2011.5C46%2011.7761424%2045.7761424%2012%2045.5%2012L25.5%2012%2025.5%2012C25.2238576%2012%2025%2011.7761424%2025%2011.5L25%2011.5%2025%2011.5C25%2011.2238576%2025.2238576%2011%2025.5%2011zM12.5%2021L45.5%2021%2045.5%2021C45.7761424%2021%2046%2021.2238576%2046%2021.5L46%2021.5%2046%2021.5C46%2021.7761424%2045.7761424%2022%2045.5%2022L12.5%2022%2012.5%2022C12.2238576%2022%2012%2021.7761424%2012%2021.5L12%2021.5%2012%2021.5C12%2021.2238576%2012.2238576%2021%2012.5%2021zM25.5%2014L45.5%2014%2045.5%2014C45.7761424%2014%2046%2014.2238576%2046%2014.5L46%2014.5%2046%2014.5C46%2014.7761424%2045.7761424%2015%2045.5%2015L25.5%2015%2025.5%2015C25.2238576%2015%2025%2014.7761424%2025%2014.5L25%2014.5%2025%2014.5C25%2014.2238576%2025.2238576%2014%2025.5%2014zM12.5%2024L45.5%2024%2045.5%2024C45.7761424%2024%2046%2024.2238576%2046%2024.5L46%2024.5%2046%2024.5C46%2024.7761424%2045.7761424%2025%2045.5%2025L12.5%2025%2012.5%2025C12.2238576%2025%2012%2024.7761424%2012%2024.5L12%2024.5%2012%2024.5C12%2024.2238576%2012.2238576%2024%2012.5%2024zM25.5%2017L33.5%2017%2033.5%2017C33.7761424%2017%2034%2017.2238576%2034%2017.5L34%2017.5%2034%2017.5C34%2017.7761424%2033.7761424%2018%2033.5%2018L25.5%2018%2025.5%2018C25.2238576%2018%2025%2017.7761424%2025%2017.5L25%2017.5%2025%2017.5C25%2017.2238576%2025.2238576%2017%2025.5%2017zM12.5%2027L45.5%2027%2045.5%2027C45.7761424%2027%2046%2027.2238576%2046%2027.5L46%2027.5%2046%2027.5C46%2027.7761424%2045.7761424%2028%2045.5%2028L12.5%2028%2012.5%2028C12.2238576%2028%2012%2027.7761424%2012%2027.5L12%2027.5%2012%2027.5C12%2027.2238576%2012.2238576%2027%2012.5%2027zM12.5%2030L26.5%2030%2026.5%2030C26.7761424%2030%2027%2030.2238576%2027%2030.5L27%2030.5%2027%2030.5C27%2030.7761424%2026.7761424%2031%2026.5%2031L12.5%2031%2012.5%2031C12.2238576%2031%2012%2030.7761424%2012%2030.5L12%2030.5%2012%2030.5C12%2030.2238576%2012.2238576%2030%2012.5%2030z%22%20opacity%3D%22.5%22/%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%23E0E3E9%22%20fill-rule%3D%22nonzero%22%20points%3D%2247%200%2047%209%2056%209%22%20opacity%3D%22.341%22/%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%2355D0E0%22%20fill-rule%3D%22nonzero%22%20points%3D%220%2025%2032%2043%200%2062%22/%3E%0A%20%20%20%20%20%20%3Cpolygon%20fill%3D%22%2355D0E0%22%20fill-rule%3D%22nonzero%22%20points%3D%2264%2025%2032%2043%2064%2062%22/%3E%0A%20%20%20%20%20%20%3Cg%20fill-rule%3D%22nonzero%22%3E%0A%20%20%20%20%20%20%20%20%3Cuse%20fill%3D%22black%22%20filter%3D%22url%28%23a%29%22%20xlink%3Ahref%3D%22%23b%22/%3E%0A%20%20%20%20%20%20%20%20%3Cuse%20fill%3D%22%2347C1D1%22%20fill-rule%3D%22evenodd%22%20xlink%3Ahref%3D%22%23b%22/%3E%0A%20%20%20%20%20%20%3C/g%3E%0A%20%20%20%20%3C/g%3E%0A%20%20%3C/g%3E%0A%3C/svg%3E%0A");
        background-size: auto;
        background-size: 100px 100px;
    }

    .sender-tpl-item-description {
        display: block;
        overflow: hidden;
        padding: 11px 12px;
        font-size: 12px;
        line-height: 14px;
        color: #828b95;
    }
</style>
<?
$tabControl->Begin();
?>
<form class="js-settings-form" method="post" name="intr_opt_form" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($mid) ?>&amp;lang=<? echo LANGUAGE_ID ?>">
    <?= bitrix_sessid_post(); ?>
    <?
    $tabControl->BeginNextTab();
    ?>
    <? foreach ($optionData as $pageCode => $page) : ?>

        <div class="bx-sender-letter-template-selector">
            <div class="bx-sender-template-selector">
                <div class="sender-tpl sender-tpl-mail">
                    <div class="sender-tpl-section">
                        <div class="sender-tpl-title">
                            <?= $page['name'] ?>
                            <a href="javascript:void(0)" onClick="jsUtils.OpenWindow('/bitrix/admin/site_settings_page_form.php?lang=<?= LANGUAGE_ID ?>&amp;page_code=<?= $pageCode ?>&amp;action=edit', 900, 700);"><img src="/bitrix/themes/.default/images/actions/edit_button_dis.gif" width="15" height="15" border="0"></a>
                            <a href="javascript:void(0)" class="js-delete-button" data-code="<?= $pageCode ?>" data-type="pages"><img src="/bitrix/themes/.default/images/actions/delete_button.gif" width="15" height="15" border="0"></a>
                        </div>
                        <div class="sender-tpl-items">
                            <? foreach ($page['tabs'] as $tabCode => $tab) : ?>
                                <div class="sender-tpl-item" style="width: 25%">
                                    <div data-role="item-content" class="sender-tpl-content">
                                        <div data-role="item-title" class="sender-tpl-item-title">
                                            <span class="sender-tpl-item-title-name">
                                                <?= $tab['name'] ?>
                                                <a href="javascript:void(0)" onClick="jsUtils.OpenWindow('/bitrix/admin/site_settings_tab_form.php?lang=<?= LANGUAGE_ID ?>&amp;tab_code=<?= $tabCode ?>&amp;action=edit', 900, 700);"><img src="/bitrix/themes/.default/images/actions/edit_button_dis.gif" width="10" height="10" border="0"></a>
                                                <a href="javascript:void(0)" class="js-delete-button" data-code="<?= $tabCode ?>" data-type="tabs"><img src="/bitrix/themes/.default/images/actions/delete_button.gif" width="10" height="10" border="0"></a>
                                            </span>
                                        </div>
                                        <? foreach ($tab['items'] as $itemCode => $item) : ?>
                                            <div data-role="item-desc" class="sender-tpl-item-description">
                                                <?= $item['name'] ?>
                                                <a href="javascript:void(0)" onClick="jsUtils.OpenWindow('/bitrix/admin/site_settings_item_form.php?lang=<?= LANGUAGE_ID ?>&amp;item_code=<?= $itemCode ?>&amp;action=edit', 900, 700);"><img src="/bitrix/themes/.default/images/actions/edit_button_dis.gif" width="10" height="10" border="0"></a>
                                                <a href="javascript:void(0)" class="js-delete-button" data-code="<?= $itemCode ?>" data-type="items"><img src="/bitrix/themes/.default/images/actions/delete_button.gif" width="10" height="10" border="0"></a>
                                            </div>
                                        <? endforeach; ?>
                                        <div data-role="item-desc" class="sender-tpl-item-description">
                                            <input type="button" title="Добавить элемент" value="+" onClick="jsUtils.OpenWindow('/bitrix/admin/site_settings_item_form.php?lang=<?= LANGUAGE_ID ?>&amp;parent=<?= $tabCode ?>', 900, 700);">
                                        </div>
                                    </div>
                                </div>
                            <? endforeach; ?>
                            <div class="sender-tpl-item">
                                <div data-role="item-content" class="sender-tpl-content">
                                    <input type="button" title="Добавить вкладку" value="Добавить вкладку" onClick="jsUtils.OpenWindow('/bitrix/admin/site_settings_tab_form.php?lang=<?= LANGUAGE_ID ?>&amp;parent=<?= $pageCode ?>&amp;action=create', 900, 700);">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <? endforeach; ?>

    <? $tabControl->Buttons(); ?>
    <input type="button" title="Добавить страницу" value="Добавить страницу" onClick="jsUtils.OpenWindow('/bitrix/admin/site_settings_page_form.php?lang=<?= LANGUAGE_ID ?>&amp;action=create', 900, 700);">
    <? $tabControl->End(); ?>
</form>

<script type="text/javascript">
    let data = $('.js-settings-form').serializeArray();
    const route = "/bitrix/services/main/ajax.php";

    $('.js-delete-button').click(function(event) {
        event.preventDefault();

        let deleteData = [];

        let type = $(this).data('type');
        let code = $(this).data('code');

        deleteData.push({
            'name': 'type',
            'value': type
        });
        deleteData.push({
            'name': 'code',
            'value': code
        });
        deleteData.push({
            'name': 'action',
            'value': 'site:settings.api.option.delete'
        });

        $.get(route, deleteData)
            .success(function(response) {
                window.location.reload();
            });
    });
</script>