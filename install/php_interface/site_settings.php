<?

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
if (!$USER->IsAdmin()) {
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}
$request = Application::getInstance()->getContext()->getRequest();
$typeOptions = SiteOptions::getFields($request["type"]);
$APPLICATION->SetTitle($typeOptions["name"]);

$LHE = new CHTMLEditor;
$LHE->Show(["display" => false]);

$arTabs = [];
$arItems = [];
if (!empty($typeOptions["tabs"])) {
	foreach ($typeOptions["tabs"] as $code => $tab) {
		$arTabs[] = ["DIV" => $code, "TAB" => $tab['name'], "ICON" => "main_user_edit", "TITLE" => $tab['name'], "ITEMS" => $tab["items"]];
		foreach ($tab["items"] as $item) {
			$arItems[$item["code"]] = $item;
		}
	}
} else {
	$arTabs[] = ["DIV" => "editMain", "TAB" => "Значения свойств", "ICON" => "main_user_edit", "TITLE" => "Значения свойств"];
}

CJSCore::Init(['jquery']);

$tabControl = new CAdminTabControl("tabControl", $arTabs);
if (isset($request["opts_save"])) {
	if (isset($request[SiteOptions::$name . "_del"])) {
		foreach ($request[SiteOptions::$name . "_del"] as $optionName => $value) {
			if (is_array($value)) {
				foreach ($value as $key => $val) {
					CFile::Delete($request[SiteOptions::$name][$optionName][$key]);
				}
			} else {
				CFile::Delete($value);
				SiteOptions::setOptionValue($optionName, "");
			}
		}
	}

	foreach ($request["opt_values"] as $code => $value) {
		$type = $arItems[$code]["type"];
		if ($type == "file") {
			SiteOptions::saveFile($code, $value);
		} else {
			SiteOptions::setOptionValue($code, $value);
		}
	}
}
?>
<style>
	.copyCode {
		display: inline-block;
		width: 21px;
		height: 20px;
		background: url(/bitrix/panel/main/images/icons-sprite-14.png?3) no-repeat 0px -198px;
		vertical-align: bottom;
		cursor: pointer;
	}

	[name^="opt_values_descr"] {
		display: block !important;
	}
</style>
<script>
	$(document).ready(function() {
		let codeTemplate = 'SiteOptions::getValue(\'#code#\')';
		$('.copyCode').click(function() {
			codeString = codeTemplate.replace('#code#', $(this).data('code'));
			navigator.clipboard.writeText(codeString);
		});
	})
</script>
<form method="post" action="<?= $APPLICATION->GetCurPageParam(); ?>" enctype="multipart/form-data" name="post_form">
	<?= bitrix_sessid_post(); ?>
	<input type="hidden" name="lang" value="<?= LANG ?>">
	<? $tabControl->Begin(); ?>
	<? foreach ($arTabs as $arTab) : ?>
		<? $tabControl->BeginNextTab(); ?>
		<? foreach ($arTab["ITEMS"] as $tabCode => $option) : ?>
			<? if ($option["parent"] == $arTab["DIV"] || !isset($option["parent"]) || !isset($typeOptions["tabs"][$option["parent"]])) : ?>
				<?= SiteOptions::getFieldTemplate($option); ?>
			<? endif;  ?>
		<? endforeach; ?>
	<? endforeach; ?>
	<?
	$tabControl->Buttons();
	?>
	<input class="adm-btn-green" type="submit" name="opts_save" value="Сохранить">
	<?
	$tabControl->End();
	?>
</form>

<script>
	$('.js-add-option-row').click(function(event) {
		let field = $(this).closest('tbody').find('tr').clone()[0];
		$(field).closest('input').val('');
		$($(field).find('td.adm-detail-content-cell-l')).html('');
		$(this).closest('tr').before($(field).get());
	});
</script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
