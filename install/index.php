<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\ModuleManager;

define('ITM_MODULE_ID', 'site.settings');

if (class_exists('site_settings')) {
    return;
}

class site_settings extends CModule
{
    var $MODULE_ID = "site.settings";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    public function __construct()
    {
        $arModuleVersion = [];

        include __DIR__ . '/version.php';

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_ID = ITM_MODULE_ID;
        $this->MODULE_NAME = 'Модуль для редактирования блоков сайта';
        $this->MODULE_DESCRIPTION = 'Служебный модуль для magii сайта';
        $this->PARTNER_NAME = 'Чипса';
        $this->PARTNER_URI = 'https://chipsa.ru/';
    }

    function site_settingsehehe()
    {
        $arModuleVersion = [
            "VERSION" => "0.3.5",
            "VERSION_DATE" => "04.10.2021"
        ];

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        $this->MODULE_NAME = "Модуль для редактирования блоков сайта";
        $this->MODULE_DESCRIPTION = "";
    }

    function InstallFiles()
    {
        if (!file_exists($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/settings")) {
            CopyDirFiles(
                $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/site.settings/install/settings",
                $_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/settings",
                true,
                true
            );
        }
        CopyDirFiles(
            $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/site.settings/install/admin",
            $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin",
            true,
            true
        );
        CopyDirFiles(
            $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/site.settings/install/php_interface",
            $_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/admin",
            true,
            true
        );
        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFilesEx("/local/php_interface/settings");
        return true;
    }

    function DoInstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        $this->InstallFiles();
        ModuleManager::RegisterModule($this->MODULE_ID);
    }

    function DoUninstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        $this->UnInstallFiles();
        ModuleManager::UnRegisterModule($this->MODULE_ID);
    }
}
