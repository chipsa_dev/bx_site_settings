<?

/**
 * Возвращаем массив, вида:
 * [
 *     'pages' => array(
 *         'pageCode' => 'Название страницы',
 * 			...
 *     ),
 *     'tabs' => array(
 *         'tabCode' => array(
 *             'name' => 'Название таба',
 *             'parent' => 'Код родительской страницы',
 *         ),
 * 			...
 *     ),
 *     'items' => array(
 *         'itemCode' => array(
 *             'name' => 'Название свойства',
 *             'type' => 'Тип свойства', //string, text, html, file, list, boolean
 *             'parent' => 'Код родительского таба',
 *             'multiple' => 'y/n', // множественное (доступно у всех, кроме boolean)
 *             'values' => [ // для типа list
 *                 // список значений
 *             ]
 *         ),
 * 			...
 *     ),
 * ];
 */

return [];
